

# Firecase

To run the project use `npm start`.

Tech Stack used for the Project
- Nx
- Angular 13
- Ngrx
- RXJS
- Tailwind
- Apollo

### Limitation I had to consider

The cookie from Steam was always being set at a different domain and trying to change the return url was breaking the behaviour, I resorted into opening the Steam Community link in a new Tab, but this unfortunately meant that the user was still not logged in after sessionToken is acquired.

In the project above after clicking on the link and logging in I still need to refresh the page for the token to take action.

Hope that was sufficient. In case I am missing something please let me know.
