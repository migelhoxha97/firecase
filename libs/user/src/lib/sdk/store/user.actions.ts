import { createAction } from '@ngrx/store';
import { CurrentUser, Wallet } from '../services/currentUser.model';

const ACTIONS_PREFIX = '[User]';

export namespace UserActions {
	export const get = createAction(`${ACTIONS_PREFIX} Get`);

	export const getSuccess = createAction(
		`${ACTIONS_PREFIX} Get success`,
		(currentUser: CurrentUser): CurrentUser => currentUser
	);

	export const getFail = createAction(
		`${ACTIONS_PREFIX} Get fail`,
		(error: any) => ({ error })
	);

	export const updateWallet = createAction(
		`${ACTIONS_PREFIX} Update Wallet`,
		(updatedWallet: Wallet) => ({ updatedWallet })
	);
}
