import { createFeatureSelector, createSelector } from '@ngrx/store';
import { UserState, USER_FEATURE_KEY } from './user.reducer';
import { CurrentUser, Wallet } from '../services/currentUser.model';

export const getUserState = createFeatureSelector<UserState>(USER_FEATURE_KEY);

export namespace UserSelectors {
	export const getCurrentUser = createSelector(
		getUserState,
		(state: UserState): CurrentUser | undefined => state.currentUser
	);

	export const getMainUserBalance = createSelector(
		getUserState,
		(state: UserState): Wallet | undefined => state.currentUser?.wallets[0]
	);
}
