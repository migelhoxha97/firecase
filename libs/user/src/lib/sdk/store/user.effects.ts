import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { UserActions } from './user.actions';
import { catchError, filter, map, of, switchMap } from 'rxjs';
import { CurrentUserGQL } from '../services/currentUser.gql';
import { UpdateWalletGql } from '../services/updateWallet.gql';
import { Store } from '@ngrx/store';

@Injectable()
export class UserEffects {
  get$ = createEffect(() =>
    this.actions$.pipe(
      ofType(UserActions.get),
      switchMap(() =>
        this.currentUserGQL.fetch().pipe(
          map(response => UserActions.getSuccess(response.data)),
          catchError(error => of(UserActions.getFail(error)))
        )
      )
    )
  );

  updateBalance$ = createEffect(() =>
    this.updateWalletGQL.subscribe().pipe(
      map(response => response.data),
      filter(Boolean),
      map(({ updateWallet }) => UserActions.updateWallet(updateWallet.wallet))
    )
  );

  constructor(
    private readonly actions$: Actions,
    private readonly currentUserGQL: CurrentUserGQL,
    private readonly updateWalletGQL: UpdateWalletGql,
    private store: Store
  ) {
  }
}
