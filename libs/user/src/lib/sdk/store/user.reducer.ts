import { Action, createReducer, on } from '@ngrx/store';

import { UserActions } from './user.actions';
import { CurrentUser } from '../services/currentUser.model';

export const USER_FEATURE_KEY = 'users';

export interface UserState {
	currentUser?: CurrentUser;
	loaded: boolean;
	error?: string | null;
}

export interface UsersPartialState {
	readonly [USER_FEATURE_KEY]: UserState;
}

export const initialState: UserState = {
	loaded: false,
};

const usersReducer = createReducer(
	initialState,
	on(UserActions.get, state => ({
		...state,
		loaded: false,
		error: null,
	})),
	on(UserActions.getSuccess, (state, currentUser) => ({
		...state,
		...currentUser,
		loaded: true,
	})),
	on(UserActions.getFail, (state, { error }) => ({
		...state,
		error,
	})),
	on(UserActions.updateWallet, (state, { updatedWallet }) => {
		const currentWallets = state.currentUser?.wallets || [];
		const updatedWallets = [];
		for (const currentWallet of currentWallets) {
			if (currentWallet.id === updatedWallet.id) {
				updatedWallets.push({ ...currentWallet, amount: updatedWallet.amount });
			} else {
				updatedWallets.push(currentWallet);
			}
		}

		return {
			...state,
			currentUser: {
				...(state.currentUser as CurrentUser),
				wallets: updatedWallets,
			},
		};
	})
);

export function reducer(state: UserState | undefined, action: Action) {
	return usersReducer(state, action);
}
