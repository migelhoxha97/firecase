import { Injectable } from '@angular/core';
import { gql } from 'apollo-angular';
import { Subscription } from 'apollo-angular';
import { UpdatedWallet } from './currentUser.model';

@Injectable({ providedIn: 'root' })
export class UpdateWalletGql extends Subscription<UpdatedWallet> {
	override document = gql`
		subscription OnUpdateWallet {
			updateWallet {
				wallet {
					id
					amount
					name
				}
			}
		}
	`;
}
