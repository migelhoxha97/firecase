import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { CurrentUser } from './currentUser.model';

@Injectable({ providedIn: 'root' })
export class CurrentUserGQL extends Query<CurrentUser> {
	override document = gql`
		query {
			currentUser {
				id
				name
				wallets {
					id
					amount
					currency
				}
			}
		}
	`;
}
