export interface CurrentUser {
	readonly id: string;
	readonly name: string;
	readonly wallets: Wallet[];
}

export interface Wallet {
	readonly id: string;
	readonly amount: number;
	readonly currency: string;
}

export interface UpdatedWallet {
	updateWallet: Record<'wallet', Wallet>;
}
