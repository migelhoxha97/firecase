import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromUser from './sdk/store/user.reducer';
import { UserEffects } from './sdk/store/user.effects';
import { CurrentUserGQL } from './sdk/services/currentUser.gql';

@NgModule({
	imports: [
		CommonModule,
		StoreModule.forFeature(fromUser.USER_FEATURE_KEY, fromUser.reducer),
		EffectsModule.forFeature([UserEffects]),
	],
	providers: [CurrentUserGQL],
})
export class UserModule {}
