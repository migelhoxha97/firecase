import { Component } from '@angular/core';

@Component({
	selector: 'fc-box-layout-container',
	templateUrl: './box-layout.container.html',
	styleUrls: ['./box-layout.container.scss'],
})
export class BoxLayoutContainer {}
