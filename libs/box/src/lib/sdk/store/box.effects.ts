import { Injectable } from '@angular/core';
import { Actions, createEffect, ofType } from '@ngrx/effects';

import { BoxActions } from './box.actions';
import { catchError, filter, map, of, switchMap, tap } from 'rxjs';
import { BoxGql } from '../services/box.gql';
import { OpenBoxGql } from '../services/openBox.gql';
import { OpenBoxInput } from '../services/box.model';

@Injectable()
export class BoxEffects {
	getAll$ = createEffect(() =>
		this.actions$.pipe(
			ofType(BoxActions.getAll),
			switchMap(() =>
				this.boxGql.fetch().pipe(
					map(response => BoxActions.getAllSuccess(response.data)),
					catchError(error => of(BoxActions.getAllFail(error)))
				)
			)
		)
	);

	openBox$ = createEffect(() =>
		this.actions$.pipe(
			ofType(BoxActions.openBox),
			switchMap(({ boxId }) =>
				// Hard-coding the other values for the testing
				this.openBoxGql
					.openBox({ amount: 1, boxId: boxId, multiplierBoxBet: 1 })
					.pipe(
						map(response => response.data),
						filter(Boolean),
						map(openBoxRewards => BoxActions.openBoxSuccess(openBoxRewards)),
						catchError(error => of(BoxActions.getAllFail(error)))
					)
			)
		)
	);

	constructor(
		private readonly actions$: Actions,
		private readonly boxGql: BoxGql,
		private readonly openBoxGql: OpenBoxGql
	) {}
}
