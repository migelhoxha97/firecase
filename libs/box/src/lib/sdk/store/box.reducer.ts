import { createEntityAdapter, EntityAdapter, EntityState } from '@ngrx/entity';
import { Action, createReducer, on } from '@ngrx/store';

import { BoxActions } from './box.actions';
import { BoxNode, OpenBoxRewards } from '../services/box.model';

export const BOXES_FEATURE_KEY = 'boxes';

export interface State extends EntityState<BoxNode> {
	selectedId?: string | number;
	loaded: boolean;
	error?: string | null;
	lastBoxOpening?: OpenBoxRewards;
	isBusy: boolean;
}

export interface BoxesPartialState {
	readonly [BOXES_FEATURE_KEY]: State;
}

export const boxNodeEntityAdapter: EntityAdapter<BoxNode> =
	createEntityAdapter<BoxNode>();

export const initialState: State = boxNodeEntityAdapter.getInitialState({
	loaded: false,
  isBusy: false
});

const boxReducer = createReducer(
	initialState,
	on(BoxActions.getAll, state => ({
		...state,
		loaded: false,
		error: null,
	})),
	on(BoxActions.getAllSuccess, (state, { edges }) => {
		const boxNodes = edges.map(val => val.node);
		return boxNodeEntityAdapter.setAll(boxNodes, { ...state, loaded: true });
	}),
	on(BoxActions.getAllFail, (state, { error }) => ({
		...state,
		error,
	})),
	on(BoxActions.setSelectedId, (state, { id }) => ({
		...state,
		selectedId: id,
	})),
  on(BoxActions.openBox, (state) => ({
    ...state,
    isBusy: true,
  })),
	on(BoxActions.openBoxSuccess, (state, { openBoxRewards }) => ({
		...state,
		lastBoxOpening: openBoxRewards,
    isBusy: false
  })),
	on(BoxActions.clearLastOpenedBox, state => ({
		...state,
		lastBoxOpening: undefined,
    isBusy: false
  }))
);

export function reducer(state: State | undefined, action: Action) {
	return boxReducer(state, action);
}
