import { createAction } from '@ngrx/store';
import { IBoxes, OpenBoxRewards } from '../services/box.model';

const ACTIONS_PREFIX = '[Box]';

export namespace BoxActions {
	export const getAll = createAction(`${ACTIONS_PREFIX} Get`);

	export const getAllSuccess = createAction(
		`${ACTIONS_PREFIX} Get All success`,
		(boxes: IBoxes) => boxes.boxes
	);

	export const getAllFail = createAction(
		`${ACTIONS_PREFIX} Get All fail`,
		(error: any) => ({ error })
	);

	export const setSelectedId = createAction(
		`${ACTIONS_PREFIX} Set selected id`,
		(id: number) => ({ id })
	);

	export const openBox = createAction(
		`${ACTIONS_PREFIX} Open`,
		(boxId: string) => ({ boxId })
	);

	export const openBoxSuccess = createAction(
		`${ACTIONS_PREFIX} Open success`,
		(openBoxRewards: OpenBoxRewards) => ({ openBoxRewards })
	);

	export const openBoxFail = createAction(
		`${ACTIONS_PREFIX} Open fail`,
		(error: any) => ({ error })
	);

	export const clearLastOpenedBox = createAction(
		`${ACTIONS_PREFIX} clear last Opened Box`
	);
}
