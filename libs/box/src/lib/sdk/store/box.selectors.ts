import { createFeatureSelector, createSelector } from '@ngrx/store';
import { State, BOXES_FEATURE_KEY, boxNodeEntityAdapter } from './box.reducer';

export const getBoxesState = createFeatureSelector<State>(BOXES_FEATURE_KEY);

const { selectAll, selectEntities } = boxNodeEntityAdapter.getSelectors();

export namespace BoxSelector {
	export const getBoxesLoaded = createSelector(
		getBoxesState,
		(state: State) => state.loaded
	);

	export const getBoxesError = createSelector(
		getBoxesState,
		(state: State) => state.error
	);

	export const getAllBoxes = createSelector(getBoxesState, (state: State) =>
		selectAll(state)
	);

	export const getBoxesEntities = createSelector(
		getBoxesState,
		(state: State) => selectEntities(state)
	);

	export const getSelectedId = createSelector(
		getBoxesState,
		(state: State) => state.selectedId
	);

	export const getSelected = createSelector(
		getBoxesEntities,
		getSelectedId,
		(entities, selectedId) => (selectedId ? entities[selectedId] : undefined)
	);

	export const getOpenedBoxRewards = createSelector(
		getBoxesState,
		(state: State) => state.lastBoxOpening?.openBox.boxOpenings
	);

  export const getBoxBusyState = createSelector(
    getBoxesState,
    (state: State) => state.isBusy
  );
}
