export interface BoxNode {
	id: string;
	name: string;
	iconUrl: string;
	cost: number;
}

export interface Edge {
	node: BoxNode;
}

export interface Boxes {
	edges: Edge[];
}

export interface IBoxes {
	boxes: Boxes;
}

export interface OpenBoxInput {
	boxId: any;
	amount: number;
	multiplierBoxBet: number;
}

export interface ItemVariant {
	name: string;
	value: number;
}

export interface BoxOpenings {
	id: string;
	itemVariant?: ItemVariant;
}

export interface OpenBoxRewards {
	openBox: Record<'boxOpenings', BoxOpenings[]>;
}
