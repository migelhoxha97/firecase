import { Injectable } from '@angular/core';
import { Apollo, gql } from 'apollo-angular';
import { OpenBoxInput, OpenBoxRewards } from './box.model';

const queryGenerator = ({
	boxId,
	multiplierBoxBet,
	amount,
}: OpenBoxInput) => gql`
mutation OpenBox($input: OpenBoxInput! = { amount: ${amount},boxId: "${boxId}", multiplierBoxBet: ${multiplierBoxBet}}) {
  openBox(input: $input) {
    boxOpenings {
      id
      itemVariant {
        id
        name
        value
      }
    }
  }
}
`;

@Injectable({ providedIn: 'root' })
export class OpenBoxGql {
	constructor(private apollo: Apollo) {}

	openBox(variables: OpenBoxInput) {
		return this.apollo.mutate<OpenBoxRewards>({
			variables,
			mutation: queryGenerator(variables),
		});
	}
}
/* Technically the approach above is not right but for some reason i couldn't pass the string as an ID
 * and I managed to pass it with the method above, but here below is the code i was using any feedback on that is welcome*/
// import { Injectable } from '@angular/core';
// import { gql, Mutation } from 'apollo-angular';
// import { OpenBoxInput, OpenBoxRewards } from './box.model';
//
// @Injectable({ providedIn: 'root' })
// export class OpenBoxGql extends Mutation<OpenBoxRewards, OpenBoxInput> {
//   override document = gql`
//     mutation OpenBox($input: OpenBoxInput!) {
//   openBox(input: $input) {
//     boxOpenings {
//       id
//       itemVariant {
//         id
//         name
//         value
//       }
//     }
//   }
// }
// `;
// }
