import { Injectable } from '@angular/core';
import { gql, Query } from 'apollo-angular';
import { IBoxes } from './box.model';

@Injectable({ providedIn: 'root' })
export class BoxGql extends Query<IBoxes> {
	override document = gql`
		{
			boxes(free: false, purchasable: true, openable: true) {
				edges {
					node {
						id
						name
						iconUrl
						cost
					}
				}
			}
		}
	`;
}
