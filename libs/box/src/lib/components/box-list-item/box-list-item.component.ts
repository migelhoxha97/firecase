import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { BoxNode } from '../../sdk/services/box.model';

@Component({
	selector: 'fc-box-list-item',
	templateUrl: './box-list-item.component.html',
	styleUrls: ['./box-list-item.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BoxListItemComponent {
	@Input() box: BoxNode | undefined;
}
