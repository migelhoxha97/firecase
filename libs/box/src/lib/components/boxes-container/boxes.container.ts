import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { BoxNode } from '../../sdk/services/box.model';
import { BoxSelector } from '../../sdk/store/box.selectors';

@Component({
	selector: 'fc-boxes-container',
	templateUrl: './boxes.container.html',
	styleUrls: ['./boxes.container.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BoxesContainer implements OnInit {
	boxes$: Observable<BoxNode[]> | undefined;
	boxesLoaded$: Observable<boolean> | undefined;
	constructor(private store: Store) {}

	ngOnInit(): void {
		this.boxes$ = this.store.select(BoxSelector.getAllBoxes);
		this.boxesLoaded$ = this.store.select(BoxSelector.getBoxesLoaded);
	}
}
