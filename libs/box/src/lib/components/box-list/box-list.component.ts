import {
	ChangeDetectionStrategy,
	Component,
	Input,
	OnChanges,
} from '@angular/core';
import { BoxNode } from '../../sdk/services/box.model';

@Component({
	selector: 'fc-box-list',
	templateUrl: './box-list.component.html',
	styleUrls: ['./box-list.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BoxListComponent {
	@Input() boxes: BoxNode[] | null | undefined;
	filters = ['All', 'Open', 'Completed'];
	selectedFilter = 0;
}
