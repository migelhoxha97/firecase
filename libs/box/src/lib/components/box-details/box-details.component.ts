import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';
import { combineLatest, map, Observable, Subscription } from 'rxjs';
import { BoxNode, BoxOpenings } from '../../sdk/services/box.model';
import { BoxSelector } from '../../sdk/store/box.selectors';
import { BoxActions } from '../../sdk/store/box.actions';
import { UserSelectors } from '@firecase/user';

@Component({
  selector: 'fc-box-details',
  templateUrl: './box-details.component.html',
  styleUrls: ['./box-details.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BoxDetailsComponent implements OnInit, OnDestroy {
  box$: Observable<BoxNode | undefined> | undefined;
  data$$: Subscription | undefined;
  boxRewards$: Observable<BoxOpenings[] | undefined> | undefined;
  isLayoutBusy$: Observable<boolean> | undefined;
  isAuthenticated$: Observable<boolean> | undefined;

  constructor(private route: ActivatedRoute, private store: Store) {
  }

  ngOnInit() {
    this.boxRewards$ = this.store.select(BoxSelector.getOpenedBoxRewards);
    this.data$$ = this.route.params.subscribe(({ id }) => {
      this.store.dispatch(BoxActions.setSelectedId(id));
    });

    this.box$ = this.store.select(BoxSelector.getSelected);
    this.isLayoutBusy$ = combineLatest([
      this.store.select(BoxSelector.getBoxBusyState),
      this.store.select(BoxSelector.getBoxesLoaded)
    ]).pipe(
      map(([isBusy, isLoaded]) => isBusy || !isLoaded)
    );

   this.isAuthenticated$ = this.store.select(UserSelectors.getCurrentUser)
     .pipe(map(user => !!user)
     );
  }

  ngOnDestroy(): void {
    this.store.dispatch(BoxActions.clearLastOpenedBox());
    this.data$$?.unsubscribe();
  }

  openBox(boxId: string) {
    this.store.dispatch(BoxActions.openBox(boxId));
  }
}
