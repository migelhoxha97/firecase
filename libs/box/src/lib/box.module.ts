import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromBoxes from './sdk/store/box.reducer';
import { BoxEffects } from './sdk/store/box.effects';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { BoxesContainer } from './components/boxes-container/boxes.container';
import { BoxListComponent } from './components/box-list/box-list.component';
import { BoxListItemComponent } from './components/box-list-item/box-list-item.component';
import { BoxDetailsComponent } from './components/box-details/box-details.component';
import { BoxLayoutContainer } from './layouts/box-layout-container/box-layout.container';
import { CoreModule } from '@firecase/core';

const COMPONENT_DECLARATIONS = [
	BoxesContainer,
	BoxListComponent,
	BoxListItemComponent,
	BoxDetailsComponent,
	BoxLayoutContainer,
];

@NgModule({
	imports: [
		CommonModule,
		StoreModule.forFeature(fromBoxes.BOXES_FEATURE_KEY, fromBoxes.reducer),
		EffectsModule.forFeature([BoxEffects]),
		RouterModule,
		ReactiveFormsModule,
		CoreModule,
	],
	declarations: COMPONENT_DECLARATIONS,
	exports: COMPONENT_DECLARATIONS,
})
export class BoxModule {}
