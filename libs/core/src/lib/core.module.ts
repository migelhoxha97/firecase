import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HeaderComponent } from './components/header/header.component';
import { LoaderComponent } from './components/loader/loader.component';
import { RouterModule } from '@angular/router';

const COMPONENT_DECLARATIONS = [HeaderComponent, LoaderComponent];

@NgModule({
	imports: [CommonModule, RouterModule],
	declarations: COMPONENT_DECLARATIONS,
	exports: COMPONENT_DECLARATIONS,
})
export class CoreModule {}
