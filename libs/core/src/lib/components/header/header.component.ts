import { ChangeDetectionStrategy, Component } from '@angular/core';
import { LOGIN_URL } from './header.const';
import { Store } from '@ngrx/store';
import { Observable } from 'rxjs';
import { CurrentUser, UserSelectors, Wallet } from '@firecase/user';

@Component({
	selector: 'fc-header',
	templateUrl: './header.component.html',
	styleUrls: ['./header.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HeaderComponent {
	currentUser$: Observable<CurrentUser | undefined>;
	currentBalance$: Observable<Wallet | undefined>;
	loginUrl = LOGIN_URL;

	constructor(private store: Store) {
		this.currentUser$ = this.store.select(UserSelectors.getCurrentUser);
		this.currentBalance$ = this.store.select(UserSelectors.getMainUserBalance);
	}
}
