import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { BrowserModule } from '@angular/platform-browser';
import { GraphQLModule } from './graphql.module';
import { HttpClientModule } from '@angular/common/http';
import { CoreModule } from '@firecase/core';
import { UserModule } from '@firecase/user';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { environment } from '../environments/environment';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';
import { BoxModule } from '@firecase/box';
import { BoxesContainer } from '@firecase/box';
import { RouterModule } from '@angular/router';
import { BoxDetailsComponent } from '../../../../libs/box/src/lib/components/box-details/box-details.component';
import { StoreRouterConnectingModule } from '@ngrx/router-store';

@NgModule({
	declarations: [AppComponent],
	imports: [
		BrowserModule,
		GraphQLModule,
		HttpClientModule,
		CoreModule,
		UserModule,
		BoxModule,
		RouterModule.forRoot(
			[
				{ path: '', redirectTo: 'cases', pathMatch: 'full' },
				{ path: 'cases', component: BoxesContainer },
				{ path: 'cases/:id', component: BoxDetailsComponent },
			],
			{
				initialNavigation: 'enabledBlocking',
			}
		),
		StoreModule.forRoot(
			{},
			{
				metaReducers: !environment.production ? [] : [],
				runtimeChecks: {
					strictActionImmutability: true,
					strictStateImmutability: true,
				},
			}
		),
		!environment.production ? StoreDevtoolsModule.instrument() : [],
		EffectsModule.forRoot([]),
		StoreRouterConnectingModule.forRoot(),
	],
	providers: [],
	bootstrap: [AppComponent],
})
export class AppModule {}
