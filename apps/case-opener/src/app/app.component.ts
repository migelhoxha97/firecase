import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { UserActions } from '@firecase/user';
import { BoxActions } from '@firecase/box';

@Component({
	selector: 'firecase-root',
	templateUrl: './app.component.html',
	styleUrls: ['./app.component.scss'],
	changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppComponent implements OnInit {
	constructor(private store: Store) {}

	ngOnInit() {
		this.store.dispatch(UserActions.get());
		this.store.dispatch(BoxActions.getAll());
	}
}
