import { NgModule } from '@angular/core';
import { APOLLO_OPTIONS, ApolloModule } from 'apollo-angular';
import { ApolloClientOptions, InMemoryCache, split } from '@apollo/client/core';
import { HttpLink } from 'apollo-angular/http';
import { getMainDefinition } from '@apollo/client/utilities';
import { GraphQLWsLink } from '@apollo/client/link/subscriptions';
import { createClient } from 'graphql-ws';
import { OperationDefinitionNode } from 'graphql';

const uri = 'https://api-staging.csgoroll.com/graphql';
const wsUri = 'ws://api-staging.csgoroll.com/graphql';

export function createApollo(httpLink: HttpLink): ApolloClientOptions<any> {
	const http = httpLink.create({
		uri,
		withCredentials: true,
	});

	const ws = new GraphQLWsLink(
		createClient({
			url: wsUri,
		})
	);

	const link = split(
		({ query }) => {
			const { kind, operation } = getMainDefinition(
				query
			) as OperationDefinitionNode;
			return kind === 'OperationDefinition' && operation === 'subscription';
		},
		ws,
		http
	);

	return {
		link: link,
		cache: new InMemoryCache(),
	};
}

@NgModule({
	exports: [ApolloModule],
	providers: [
		{
			provide: APOLLO_OPTIONS,
			useFactory: createApollo,
			deps: [HttpLink],
		},
	],
})
export class GraphQLModule {}
